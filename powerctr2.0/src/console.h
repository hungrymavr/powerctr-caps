/*
 * console.h
 *
 * Created: 28.08.2014 16:38:50
 *  Author: ale
 */ 


#ifndef CONSOLE_H_
#define CONSOLE_H_

#include <asf.h>
#include "i2c.h"

uint8_t hello_string[24];
uint8_t debug_msg[248];

void console_init(void);
void console_send(void);
void print(char *line);


uint16_t get_battery_voltage(void);
uint16_t get_battery_current(void);
uint16_t get_battery_temp(void);






#endif /* CONSOLE_H_ */