/*
 * i2c.c
 *
 * Created: 27.08.2014 12:11:51
 *  Author: ale
 */ 

#include <asf.h>
#include <i2c.h>
#include "console.h"
#include "battery.h"
#include "utils/mcp.h"

void i2c_init() {
	
	twi_options_t master_opt = {
		.speed     = TWI_SPEED,
		.chip      = SELF_ADDR,
		.speed_reg = TWI_BAUD(sysclk_get_cpu_hz(), TWI_SPEED)
	};
	
	sysclk_enable_peripheral_clock(&TWI_MASTER);
	twi_master_init(&TWI_MASTER, &master_opt);
	twi_master_enable(&TWI_MASTER);	
	
	sysclk_enable_peripheral_clock(&TWI_SLAVE);
	TWI_SlaveInitializeDriver(&i2c_slave, &TWI_SLAVE, *i2c_s_process);
	TWI_SlaveInitializeModule(&i2c_slave, HOST_ADDR, TWI_SLAVE_INTLVL_MED_gc);	
	
	ds_pkg.read_address = 0x00;
}

twi_package_t max8731_get_pkg(uint8_t command, uint16_t value) {
	max_pkg.command = command;
	max_pkg.value = value;
	
	twi_package_t pkg = {
		.addr_length = 0,
		.chip        = MAX8781_ADDRESS,
		.buffer      = (void *) &max_pkg,
		.length      = MAX8781_PKG_SIZE,
		.no_wait     = false
	};
	
	return pkg;
};

bool i2c_m_query_ds2782() {
	twi_package_t pkg_wr = {
		.addr_length = 0,
		.chip        = DS2782_ADDR,
		.buffer      = (void *) &ds_pkg,
		.length      = DS2782_PKG_SIZE,
		.no_wait     = false
	};
	
	if(twi_master_write(&TWI_MASTER, &pkg_wr) != STATUS_OK) return false;

	twi_package_t pkg_rd = {
		.addr		 = {0x00}, 
		.addr_length = 1,
		.chip        = DS2782_ADDR,
		.buffer      = (void *) &battery.map,
		.length      = 256,
		.no_wait     = false
	};
	twi_master_read(&TWI_MASTER, &pkg_rd);
	
	return true;
}


void i2c_s_process() {
	i2c_slave.sendData = battery.get_addr_map(i2c_slave.receivedData[0]);
}

void twi_master_c_callback() {
	static int counter = 0;
	if(++counter != 4) return;  // this call twi_master_callback() once in 4 sec.
	
	counter = 0;
	twi_master_callback();
}

void twi_master_callback() {
	battery.query();
	
	if( is_bort() ) {
		if(!battery.charge()) {								// charging..
			battery.nocharge();
		}
	}
	else {
		battery.nocharge();
		if(dev.session == USER_SESSION) return;
		if(!battery.condition())
			mcp_off();					// no charging. monitoring.
	}
}

ISR(TWIC_TWIS_vect) {
	TWI_SlaveInterruptHandler(&i2c_slave);
}