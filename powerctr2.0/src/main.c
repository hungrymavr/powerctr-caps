/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
#define F_CPU 2000000UL

#include <asf.h>
#include <util/delay.h>
#include "i2c.h"
#include "console.h"
#include "utils/mcp.h"
#include "utils/buttons.h"

int main(void) {
	board_init();
	sei();
	
	_delay_ms(50);
	
	twi_master_run();
	
	while(true) {
		wdt_reset();
	}
}

/* mcp line */
ISR(PORTA_INT0_vect) {
	dev.button = BUTTON_UNLOCK;
	
	if(is_mcp_enable() ) {  // mcp enable
		powerbutton_callback();
		if(dev.session == BORT_LAUNCH_SESSION) {
			dev.session = BORT_SESSION;
		} else {
			if(is_bort()) dev.session = BORT_SESSION;
			else		  dev.session = USER_SESSION;
		}
				
		twi_master_run();
	} else {
		if(dev.session == BORT_SHUTDOWN_SESSION) {

			powerbutton_callback();
			if(is_bort()) {
				// this delay needing by mcp
				_delay_ms(1000);
				
				mcp_on();
				return;
			}
		}
		
		dev.session = NONE_SESSION;
		/* go to initial state, if bort not enable */
		if(!is_bort())	no_twi_master_run();
	}
	
}

/* bort line */
ISR(PORTA_INT1_vect) {
	if(is_bort()) {			// bort enable
		twi_master_run();
//		if((dev.session == USER_SESSION) || (dev.session == BORT_SHUTDOWN_SESSION))
		if(dev.session == USER_SESSION)  dev.session = BORT_SESSION;

		/* turn on mcp, if it doesn't */
		if(!is_mcp_enable())	mcp_on();
	} else {
		/* go to initial state, if mcp not work */
		if(!is_mcp_enable())	no_twi_master_run();
		/* start 5 min timer*/
		else 
			if(dev.session == BORT_SESSION) {
				//tc_reset(&TCF0);
				//tc_restart(&TCF0);
				//tc_enable(&TCF0);
			}
	
	}

}

// on/off button
ISR(PORTB_INT0_vect) {
	if(dev.button == BUTTON_LOCK) return;

	dev.antibounce_object = ONOFF_BUTTON;
	
	tc_reset(&TCE1);
	tc_restart(&TCE1);
	tc_enable(&TCE1);
}

// chute
ISR(PORTF_INT0_vect) {
	if( (is_mcp_enable()) || (is_bort()) ) return;		// mcp not run and bort disable
	dev.antibounce_object = CHUTE;
	
	tc_reset(&TCE1);
	tc_restart(&TCE1);
	tc_enable(&TCE1);
}