/*
 * mcp.c
 *
 * Created: 25.09.2014 12:38:55
 *  Author: ale
 */ 

#include "asf.h"
#include "utils/mcp.h"
#include "utils/buttons.h"
#include "i2c.h"

void twi_master_run() {
	if(dev.twi_master_processing) return;
	
	dev.twi_master_processing = true;
	twi_master_callback();
	tc_reset(&TCE0);
	tc_restart(&TCE0);
	tc_enable(&TCE0);
}

void no_twi_master_run() {
	dev.twi_master_processing = false;
	tc_disable(&TCE0);
}


void mcp_on() {
	if((PORTA.IN & 0x20) == 0x00) return;
	if((PORTA.IN & 0x10) == 0x00) dev.session = BORT_LAUNCH_SESSION;
	else dev.session = USER_SESSION;
	button_power_on(0);
}

void mcp_off() {
	if(!is_mcp_enable())	return;
	if(dev.session == BORT_SHUTDOWN_SESSION) return;
	
	dev.session = BORT_SHUTDOWN_SESSION;
	
	button_power_on(65500);

	dev.time = 0;
	
	tc_reset(&TCC0);
	tc_restart(&TCC0);
	tc_enable(&TCC0);
}

void mcp_timeout_callback() {
	if( (dev.session != BORT_SHUTDOWN_SESSION) || (is_bort()) ) { 
		tc_disable(&TCC0);
		dev.session = BORT_SESSION;
		return;
	}
		
	if(++dev.time == 120) {
		button_power_on(0);
		tc_disable(&TCC0);
	}
}

void mcp_power_timeout_callback() {
	static int time = 0;
	if(!is_bort()) time++;
	else {
		time = 0;
		tc_disable(&TCF0);
		return;
	}
	
	/* 1 time == 1 sec */
	/* 300 time == 5 min */
	if(time == 300) {
		if(dev.session == BORT_SESSION)	mcp_off();
		time = 0;
		tc_disable(&TCF0);
	}
}

inline bool is_mcp_enable() {
	return ((PORTA.IN & 0x20) == 0x00) ? true : false;
}

inline bool is_charging() {
	return ((PORTA.IN & 0x10) == 0x00) ? true : false;
}

inline bool is_powergood() {
	return (PORTD.IN & 0x80) ? true : false;
}

inline bool is_bort() {
	return ((PORTA.IN & 0x10) == 0x00) ? true : false;
}